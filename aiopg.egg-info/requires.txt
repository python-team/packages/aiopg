psycopg2-binary>=2.9.5
async_timeout<5.0,>=3.0

[sa]
sqlalchemy[postgresql_psycopg2binary]<2.1,>=1.4
